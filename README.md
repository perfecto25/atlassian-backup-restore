#Atlassian Backup & Restore scripts

## backup Jira
```
runTask "Creating backup destination" "mkdir -p $BACKUP/$APP_HOME/db_dump" || return
runTask "Creating mysqldump"    "mysqldump -u $APP --single-transaction --add-drop-database --databases $APP --result-file=$BACKUP/$APP_HOME/db_dump/${APP}_db.sql"
runTask "Expending backup destination tree"    "mkdir -p $CONFIG $BACKUP/$APP_HOME/{plugins,data} " || return
runTask "Syncing db config"                    "$RSYNC {$APP_HOME,$BACKUP/$APP_HOME}/dbconfig.xml"
runTask "Syncing jira-config"                  "$RSYNC {$APP_HOME,$BACKUP/$APP_HOME}/jira-config.properties"
runTask "Syncing plugins"                      "$RSYNC --delete {$APP_HOME,$BACKUP/$APP_HOME}/plugins/"
runTask "Syncing data (this may take awhile)"  "$RSYNC --delete {$APP_HOME,$BACKUP/$APP_HOME}/data/"
runTask "Syncing SSO config"                   "$RSYNC $APP_INSTALL/atlassian-$APP/WEB-INF/classes/{crowd.properties,seraph-config.xml} $CONFIG/"
```

## backup confluence
```
runTask "Creating backup destination" "mkdir -p $BACKUP/$APP_HOME/db_dump" || return 
runTask "Creating mysqldump"    "mysqldump -u $APP --single-transaction --add-drop-database --databases $APP --result-file=$BACKUP/$APP_HOME/db_dump/${APP}_db.sql"

runTask "Expending backup destination tree"    "mkdir -p $CONFIG $BACKUP/$APP_HOME/attachments" || return
runTask "Syncing confluence config"            "$RSYNC {$APP_HOME,$BACKUP/$APP_HOME}/confluence.cfg.xml"
runTask "Syncing attachments"                  "$RSYNC --delete {$APP_HOME,$BACKUP/$APP_HOME}/attachments/"
runTask "Syncing SSO config"                   "$RSYNC $APP_INSTALL/$APP/WEB-INF/classes/{crowd.properties,seraph-config.xml} $CONFIG/"
```

## backup bitbucket
```
# remove old tars
tars=$(find $BACKUP/backups -type f -mtime +3 -print)
for tar in $tars; 
do
runTask "deleting old tar: $tar" "rm $tar"
done
runTask "Running Backup Client for Bitbucket"  "cd $BIN_DIR/bitbucket_backup_client && $JAVA_HOME/bin/java -jar bitbucket-backup-client.jar"
;;
```